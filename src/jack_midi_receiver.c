#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <jack/jack.h>
#include <jack/midiport.h>
#include <jack/ringbuffer.h>

#define INPUT_PORT_NAME "midi_in"
#define PROGRAM_NAME "jack-midi-log"


jack_client_t *jack_client = NULL;
jack_port_t *input_port;
int exit_status = 0;  // jack_midi_receiver thread exit status
pthread_mutex_t thread_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t should_exit = PTHREAD_COND_INITIALIZER;

typedef struct __attribute__((__packed__)) {
    jack_nframes_t time;
    uint8_t size;
    uint8_t data[3];
} buf_midi_event_t;


static void warn(const char *msg) {
    fprintf(stderr, "%s\n", msg);
}


int process_midi_input(jack_nframes_t nframes, void *arg) {
    jack_ringbuffer_t *rb = (jack_ringbuffer_t *)arg;
    int read, events, i;
    void *port_buffer;
    jack_midi_event_t event;
    buf_midi_event_t buf_event;
    jack_nframes_t last_frame_time;
    static int time_of_first_event = -1;

    last_frame_time = jack_last_frame_time(jack_client);
    port_buffer = jack_port_get_buffer(input_port, nframes);

    if (port_buffer == NULL) {
        warn("jack_port_get_buffer failed, cannot receive anything.");
        return 0;
    }

    events = jack_midi_get_event_count(port_buffer);

    for (i = 0; i < events; i++) {
        read = jack_midi_event_get(&event, port_buffer, i);

        if (read) {
            warn("jack_midi_event_get failed, RECEIVED NOTE LOST.");
            continue;
        }

        /* Ignore System Exclusive and System Real Time messages. */
        if (event.buffer[0] == 0xF0 || event.buffer[0] >= 0xF8) {
            continue;
        }

        /* We don't handle events with more than 3 bytes. */
        if (event.size > 3) {
            continue;
        }

        /* First event received? */
        if (time_of_first_event == -1) {
            time_of_first_event = last_frame_time + event.time;
        }

        buf_event.time = last_frame_time + event.time - (jack_nframes_t)time_of_first_event;
        buf_event.size = (uint8_t)event.size;
        buf_event.data[0] = (uint8_t)event.buffer[0];
        buf_event.data[1] = (uint8_t)event.buffer[1];
        buf_event.data[2] = (uint8_t)event.buffer[2];
        jack_ringbuffer_write(rb, (char *)&buf_event, sizeof(buf_midi_event_t));
    }

    return 0;
}


static int init_jack(void *arg) {
    int err;

    jack_client = jack_client_open(PROGRAM_NAME, JackNullOption, NULL);

    if (jack_client == NULL) {
        warn("Could not connect to the JACK server; run jackd first?");
        return 1;
    }

    err = jack_set_process_callback(jack_client, process_midi_input, arg);

    if (err) {
        warn("Could not register JACK process callback.");
        return 1;
    }

    input_port = jack_port_register(jack_client, INPUT_PORT_NAME, JACK_DEFAULT_MIDI_TYPE,
        JackPortIsInput, 0);

    if (input_port == NULL) {
        warn("Could not register JACK input port.");
        return 1;
    }

    if (jack_activate(jack_client)) {
        warn("Cannot activate JACK client.");
        return 1;
    }

    return 0;
}


static void cancel_handler(void *) {
    if (jack_client != NULL) {
        warn("Closing JACK client.");
        jack_client_close(jack_client);
        jack_client = NULL;
    }
}


void stop_receiver_thread() {
    pthread_mutex_lock(&thread_lock);
    pthread_cond_signal(&should_exit);
    pthread_mutex_unlock(&thread_lock);
}


void * jack_midi_receiver_thread(void *arg) {
    jack_ringbuffer_t *rb = (jack_ringbuffer_t *)arg;
    sigset_t set;

    sigemptyset(&set);
    sigaddset(&set, SIGINT);
    sigaddset(&set, SIGTERM);

    exit_status = pthread_sigmask(SIG_BLOCK, &set, NULL);

    if (exit_status != 0) {
        warn("Could not set signal mask.");
        pthread_exit(&exit_status);
    }

    exit_status = init_jack(rb);

    if (exit_status != 0) {
        warn("Failed to initialize JACK client.");
        pthread_exit(&exit_status);
    }

    pthread_cleanup_push(cancel_handler, NULL);

    pthread_cond_wait(&should_exit, &thread_lock);

    warn("Exit condition signalled.");
    warn("Exiting receiver thread.");

    pthread_cleanup_pop(1);
    pthread_exit(&exit_status);
}
