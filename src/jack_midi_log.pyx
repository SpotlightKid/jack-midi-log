# cython: language_level=3, boundscheck=False

import array
from struct import unpack
from time import sleep

from cpython cimport array
from cython.operator cimport dereference as deref
from libc.stdint cimport uint32_t, uint8_t
from posix.unistd cimport usleep

from pthread cimport *
from jackringbuffer cimport *


cdef int BUFSIZE = 4096
cdef size_t BUF_EVENT_SIZE = 8


#cdef extern from "jack/jack.h" nogil:
#    ctypedef uint32_t jack_nframes_t

cdef extern from "jack_midi_receiver.c" nogil:
    cdef void *jack_midi_receiver_thread(void *args)
    cdef void stop_receiver_thread()
    #ctypedef packed struct buf_midi_event_t:
    #    jack_nframes_t time
    #    uint8_t size
    #    uint8_t data[3]


def main():
    """The main routine and application entry point of this module."""
    cdef pthread_t thread1
    cdef void *res
    cdef jack_ringbuffer_t *rb
    cdef char msg[8]
    #cdef buf_midi_event_t * event
    cdef size_t nbytes

    rb = jack_ringbuffer_create(BUFSIZE)
    jack_ringbuffer_reset(rb)
    print("JACK ring buffer created.")

    if pthread_create(&thread1, NULL, jack_midi_receiver_thread, rb) != 0:
        print("Could not start jack_midi_receiver thread.")
        jack_ringbuffer_free(rb)
        return 1

    print("jack_midi_receiver thread created.")

    try:
        while 1:
            nbytes = jack_ringbuffer_read_space(rb)

            if nbytes >= BUF_EVENT_SIZE:
                print("Reading %d bytes." % (nbytes,))
                while nbytes:
                    jack_ringbuffer_read(rb, <char *>msg, BUF_EVENT_SIZE)
                    nbytes -= BUF_EVENT_SIZE
                    #event = <buf_midi_event_t *>msg
                    #print(f"Time: {event.time}")
                    #print(f"Size: {event.size}")
                    #print(f"Data: {event.data!r}")
                    time, size, *data = unpack("IB3B", <bytes>msg[:BUF_EVENT_SIZE])
                    print(f"Time: {time}")
                    print(f"Size: {size}")
                    print(f"Data: {data!r}")
                    print()

            sleep(.1)
    except KeyboardInterrupt:
        print("Stopping jack_midi_receiver thread...")
        stop_receiver_thread()
        pthread_join(thread1, &res);
        print("jack_midi_receiver thread joined. Exit code: %d" % (deref(<int *>res),))

    jack_ringbuffer_free(rb)

    return 0
