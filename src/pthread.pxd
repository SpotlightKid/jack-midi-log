cdef extern from "pthread.h" nogil:
    ctypedef int pthread_t
    ctypedef struct pthread_attr_t:
        pass

    cdef int pthread_attr_destroy(pthread_attr_t *attr)
    cdef int pthread_attr_init(pthread_attr_t *attr)
    cdef int pthread_cancel(pthread_t thread)
    cdef int pthread_create(pthread_t *thread, pthread_attr_t *attr, void *(*start_routine) (void *), void *arg)
    cdef void pthread_exit(void *retval)
    cdef int pthread_join(pthread_t thread, void **retval)
    cdef pthread_t pthread_self()
