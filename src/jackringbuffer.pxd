from libc.stddef cimport size_t

cdef extern from "jack/ringbuffer.h" nogil:
    ctypedef struct jack_ringbuffer_t:
        pass
    ctypedef struct jack_ringbuffer_data_t:
        pass

    cdef jack_ringbuffer_t * jack_ringbuffer_create (size_t sz)
    cdef void jack_ringbuffer_free(jack_ringbuffer_t *rb)
    cdef void jack_ringbuffer_get_read_vector(const jack_ringbuffer_t *rb, jack_ringbuffer_data_t *vec)
    cdef void jack_ringbuffer_get_write_vector(const jack_ringbuffer_t *rb, jack_ringbuffer_data_t *vec)
    cdef size_t jack_ringbuffer_read (jack_ringbuffer_t *rb, char *dest, size_t cnt)
    cdef size_t jack_ringbuffer_peek (jack_ringbuffer_t *rb, char *dest, size_t cnt)
    cdef void jack_ringbuffer_read_advance (jack_ringbuffer_t *rb, size_t cnt)
    cdef size_t jack_ringbuffer_read_space (const jack_ringbuffer_t *rb)
    cdef int jack_ringbuffer_mlock (jack_ringbuffer_t *rb)
    cdef void jack_ringbuffer_reset (jack_ringbuffer_t *rb)
    cdef size_t jack_ringbuffer_write (jack_ringbuffer_t *rb, const char *src, size_t cnt)
    cdef void jack_ringbuffer_write_advance (jack_ringbuffer_t *rb, size_t cnt)
    cdef size_t jack_ringbuffer_write_space (const jack_ringbuffer_t *rb)
