# jack-midi-log

A command line tool to record standard MIDI files from JACK MIDI

## Build and install

```con
meson setup builddir --prefix=/usr -Dcheck-py-modules=false
meson compile -C builddir
meson install -C builddir --destdir=/tmp/jack-midi-log-root
```

## Packaging

### Python binary wheel

```con
python -m build --wheel
```

### Source distribution archive

With meson:

```con
meson dist -C builddir
```

With Python PEP-517 compatible tooling:

```con
python -m build --sdist
```

## Requirements

### Build-time Dependencies

* Git
* [JACK2] >=1.9.11 (recommended) or JACK1 >=0.125.0
* Python >= 3.7

With meson:

* [Cython]
* [meson] >=0.53.0
* [ninja]

With Python PEP-517 compatible tooling:

* [build]

Note: `python -m build` will install Cython, meson and ninja in a
transient Python virtual environment.

### Run-time Dependencies

* [appdirs] [^1]
* A running JACK server
* Python >= 3.7


[^1]: By default, meson will check the presence of Python packages *required at
run-time* at build-time, unless you pass `-Dcheck-py-modules=false` to
`meson setup`.


[appdirs]: https://pypi.python.org/project/appdirs
[build]: https://pypi.python.org/project/build
[Cython]: https://cython.org/
[JACK2]: https://jackaudio.org/
[meson]: https://mesonbuild.com/
[ninja]: https://ninja-build.org/
